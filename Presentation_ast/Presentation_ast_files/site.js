
/* Created 06/07/2015 by Fabian Salamo  @Yama Group */

//Define arrays for each segment:
//Arrays positions correspond to questions in this way: Q1, Q2, Q3_1, Q3_2, Q4

var segment1 = [-3.210707937, -0.45373745, -3.243144087, 0.503810151, -4.320928815];

var segment2 = [0.463654789, 0.42327923, -5.024763178, 0.934925174, -5.282146356];

var segment3 = [-1.893207684, -0.3807725, -2.412338731, 1.887851453, -4.421527587];

var segment4 = [-0.473957787, 0.628190746, -6.0500458, 0.747805451, -3.571555571];

var segmentConstants = [50.31495149, 56.43566531, 40.08208995, 51.2739782];

var currentQuestion = 1;
var partialAnswers = '';
var canContinue = false;
var savedID = 0;
var obj = {};
var saveSegment;
var launchedFromAccount = false;
var resultCode;
var answerSelection = '';
var globalSelected;
var values;
var segmentDoctor;

var answ1, answ2, answ3, answ4;

//This method calculates the value for the segment depending on the submitted answers
function getSegment1() {
    var sum = 0;
    var answersArray = partialAnswers.substring(0, partialAnswers.length - 1).split('-');
    for (var i = 0; i < answersArray.length; i++) {
        sum += parseInt(answersArray[i]) * segment1[i];
    }
    var partial = Math.exp((sum + segmentConstants[0]));
    return partial;
}

function getSegment2() {
    var sum = 0;
    var answersArray = partialAnswers.substring(0, partialAnswers.length - 1).split('-');
    for (var i = 0; i < answersArray.length; i++) {
        sum += parseInt(answersArray[i]) * segment2[i];
    }
    var partial = Math.exp((sum + segmentConstants[1]));
    return partial;
}

function getSegment3() {
    var sum = 0;
    var answersArray = partialAnswers.substring(0, partialAnswers.length - 1).split('-');
    for (var i = 0; i < answersArray.length; i++) {
        sum += parseInt(answersArray[i]) * segment3[i];
    }
    var partial = Math.exp((sum + segmentConstants[2]));
    return partial;
}

function getSegment4() {
    var sum = 0;
    var answersArray = partialAnswers.substring(0, partialAnswers.length - 1).split('-');
    for (var i = 0; i < answersArray.length; i++) {
        sum += parseInt(answersArray[i]) * segment4[i];
    }
    var partial = Math.exp((sum + segmentConstants[3]));
    return partial;
}

$(document).ready(function () {
    accountSelected();
    //callBackAccountSelected("ss");
});

//Check if user launched media from an account record and returns false if he did not
function accountSelected() {
    com.veeva.clm.getDataForCurrentObject("Account", "Id", callBackAccountSelected);
}

function callBackAccountSelected(result) {
    if (result.code == 1112) {
        //No account was selected prior to segmentation
        $(".startButton").hide();
        $(".startButtonShine").hide();
        $(".startButtonBack").hide();
        setTimeout(function () { alert(items[43]); }, 40);
    } else {
        //Normaly start the application
        launchedFromAccount = true;
        $(".wellcomeMessage").html(items[34]);
        $(".startButton").html(items[35]);
        $("#prevButton").text(items[37]);
        $("#nextButton").text(items[36]);
        $(".disclaimerTxt").text(items[49]);
    }
}

function startSegmentation() {
    if (launchedFromAccount == false) {
        //Close presentation
    } else {
        //Load the first question
        $(".disclaimerTxt").css("color", "#1c2d58");
        loadFirstQuestion();
        //hide home & show first question 
        $("#homeSlide").fadeOut(function () {
            $("#presentationSlide").fadeIn();
        })
    }
}

function loadFirstQuestion() {
    $(".titleQuestion").html(titleQuestion1);
    $(".question").html(question1);
    $(".answerContainer").html(answerSet1);
}

function next() {
    if (canContinue) {
        animateNext();
        answerSelection += globalSelected;
        $("#prevButton").removeClass("disabled");
        saveAnswer();
        $("#fadeComponents").fadeOut(700, function () {
            if ($("#nextButton").text() == items[38]) {
                setTimeout(function () {
                    showSegmentation();
                }, 1000);
            } else {
                if (currentQuestion == 1) {
                    $(".titleQuestion").html(titleQuestion2);
                    $(".question").html(question2);
                    $(".answerContainer").html(answerSet2);
                    $(".answerContainer").addClass("answerContainerZoom");
                    $(".answerContainer").addClass("centeredAnswer1");
                    if (answ2 != null) {
                        selectAnswer(answ2);
                    }
                }
                if (currentQuestion == 2) {//for Q 3.1
                    $(".titleQuestion").html(titleQuestion31);
                    $(".question").html(question31);
                    $(".subquestionContainer").show();
                    $(".subquestionContainer").html(question3);
                    $(".answerContainer").html(answerSet3);
                    $(".answerContainer").removeClass("centeredAnswer1");
                    $(".answerContainer").addClass("centeredAnswer2");
                    if (answ3 != null) {
                        selectAnswer(answ3);
                    }
                }
                if (currentQuestion == 3) { //for Q 3.2
                    $(".titleQuestion").html(titleQuestion32);
                    $(".question").html(question32);
                    $(".subquestionContainer").show();
                    $(".subquestionContainer").html(question3);
                    $(".answerContainer").html(answerSet3);
                    $(".answerContainer").removeClass("centeredAnswer2");
                    $(".answerContainer").addClass("centeredAnswer3");
                    if (answ4 != null) {
                        selectAnswer(answ4);
                    }
                    //sub-question 3.2
                }
                if (currentQuestion == 4) {
                    $(".titleQuestion").html(titleQuestion4);
                    $(".subquestionContainer").hide();
                    $(".question").html(question4);
                    $(".answerContainer").html(answerSet4);
                }
                currentQuestion = currentQuestion + 1;

                if (currentQuestion > 4) {
                    $("#nextButton").text(items[38]);
                }
                $("#fadeComponents").fadeIn();
            }
        });
        canContinue = false;
    } else {
        alert(items[39]);
    }
}

function animateNext() {
    $(".progress").animate({
        width: "+=20%",
    }, 2000, function () {
    });
}

function animatePrev() {
    $(".progress").animate({
        width: "-=20%",
    }, 2000, function () {
    });
}

function prev() {
    if (currentQuestion == 1) {
        return;
    }
    canContinue = false;
    //fade out container
    animatePrev();
    $("#fadeComponents").fadeOut(700, function () {
        $("#nextButton").text(items[36]);
        $("#nextButton").show();
        //set new content
        if (currentQuestion == 2) {
            $("#prevButton").addClass("disabled");
            $(".answerContainer").removeClass("answerContainerZoom")
            $(".titleQuestion").html(titleQuestion1);
            $(".question").html(question1);
            $(".answerContainer").html(answerSet1);
            $(".answerContainer").removeClass("centeredAnswer2");
            $(".answerContainer").removeClass("centeredAnswer1");
        }//Load question 2:
        if (currentQuestion == 3) {
            $(".question").html(question2);
            $(".subquestionContainer").hide();
            $(".answerContainer").html(answerSet2);
            $(".titleQuestion").html(titleQuestion2);
            $(".answerContainer").removeClass("centeredAnswer3");
            $(".answerContainer").addClass("centeredAnswer2");
        }
        if (currentQuestion == 4) { //for Q 3.2
            $(".question").html(question31);
            //sub-question 3.2
            $(".subquestionContainer").html(question3);
            $(".answerContainer").html(answerSet3);
            $(".titleQuestion").html(titleQuestion31);
        }
        if (currentQuestion == 5) {
            $(".subquestionContainer").show();
            $(".question").html(question32);
            $(".subquestionContainer").html(question3);
            $(".answerContainer").html(answerSet3);
            $(".titleQuestion").html(titleQuestion32);
        }
        currentQuestion = currentQuestion - 1;
        if (currentQuestion == 1) {
            $("#prevButton").addClass("disabled");
        }
        //add selected class to selected previously selected value
        var prevAnsw = getPrevAnswer();
        selectAnswer(prevAnsw);

        var aux = answerSelection.substring(0, answerSelection.length - 1);
        answerSelection = aux;
        $("#fadeComponents").fadeIn();
        var aux = partialAnswers.substring(0, partialAnswers.length - 2);
        partialAnswers = aux;
    });
}

function getPrevAnswer() {
    if (currentQuestion == 1) {
        return answ1;
    }
    if (currentQuestion == 2) {
        return answ2;
    }
    if (currentQuestion == 3) {
        return answ3;
    }
    if (currentQuestion == 4) {
        return answ4;
    }
}


function select(selected) {
    canContinue = true;
    var aux = selected.split(',')[1];
    //Sets the background for the selected item
    selectAnswer(aux);
}

function saveAnswer() {
    var elem = $(".selected").attr("id")[15];

    if (currentQuestion == 1) {
        answ1 = elem;
    }
    if (currentQuestion == 2) {
        answ2 = elem;
    }
    if (currentQuestion == 3) {
        answ3 = elem;
    }
    if (currentQuestion == 4) {
        answ4 = elem;
    }

    if (currentQuestion == 1) {
        if (elem != 1) {
            elem = 0;
        }
    }
    if (partialAnswers.length < 1) {
        partialAnswers = $("#partialResult").val();
    }
    partialAnswers += elem + '-';

    
}

function saveID(result) {
    savedID = result.ID;
}

function unSelect() {
    var partialAnswers = $("#partialResult").val();
    partialAnswers += selected + '-';
    console.log('Partial answers: ' + partialAnswers);
}

function selectAnswer(selected) {
    var answer = 'answerSelection' + selected;
    for (var i = 0; i < 8; i++) {
        $("#answerSelection" + i).removeClass("selected");
    }
    $("#" + answer).addClass("selected");
    canContinue = true;
    globalSelected = selected;
}

function testSegmentation(array) {
    partialAnswers = array;
    var s1 = getSegment1();
    var s2 = getSegment2();
    var s3 = getSegment3();
    var s4 = getSegment4();
    var total = s1 + s2 + s3 + s4;
    var p1 = Math.round((s1 * 100) / total);
    var p2 = Math.round((s2 * 100) / total);
    var p3 = Math.round((s3 * 100) / total);
    var p4 = Math.round((s4 * 100) / total);
    values = 'Segmentation: ' + p1 + '% -' + p2 + '% -' + p3 + '% -' + p4 + '%';
    var max = 0;
    var segment = 0;
    var arr = [p1, p2, p3, p4];
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
            segment = i + 1;
        }
    }
    if (segment == 1) {
        saveSegment = items[45];
    }
    if (segment == 2) {
        saveSegment = items[46];
    }
    if (segment == 3) {

        saveSegment = items[47];
    }
    if (segment == 4) {
        saveSegment = items[48];
    }
    console.log(values);
}

function showSegmentation() {
    $(".buttonsContainer").fadeOut();
    var s1 = getSegment1();
    var s2 = getSegment2();
    var s3 = getSegment3();
    var s4 = getSegment4();
    var total = s1 + s2 + s3 + s4;
    var p1 = Math.round((s1 * 100) / total);
    var p2 = Math.round((s2 * 100) / total);
    var p3 = Math.round((s3 * 100) / total);
    var p4 = Math.round((s4 * 100) / total);
    values = 'Segmentation: ' + p1 + '% -' + p2 + '% -' + p3 + '% -' + p4 + '%';
    var max = 0;
    var segment = 0;
    var arr = [p1, p2, p3, p4];
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
            segment = i + 1;
        }
    }
    if (segment == 1) {
        segmentDoctor = items[50];
        saveSegment = items[45];
    }
    if (segment == 2) {
        segmentDoctor = items[51];
        saveSegment = items[46];
    }
    if (segment == 3) {
        segmentDoctor = items[52];
        saveSegment = items[47];
    }
    if (segment == 4) {
        segmentDoctor = items[53];
        saveSegment = items[48];
    }
    com.veeva.clm.getDataForCurrentObject("Account", "Id", callBackSave);
   //storeAnswer("");
}

function callBackSave(result) {
    obj.Account_vod__c = result.Account.Id;
    obj.ABV_IE_Qualitative_Segmentation__c = saveSegment;
    com.veeva.clm.createRecord("Product_Metrics_vod__c", obj, storeAnswer);
}

function storeAnswer(result) {
    $("#thankYou").text(items[40]);
    $("#segmentTitle").text(items[41]);
    $("#segment").text(saveSegment);
    $("#segmentDoctor").text(segmentDoctor);
    $("#finalMessage").fadeIn();
}

var titleQuestion1 = items[0];
var question1 = items[1];

var answerSet1 = "";
answerSet1 += "<div class=\"answer\"  id=\"answer1\" onclick=\"select('1,1')\">";
answerSet1 += "<p>" + items[2] + "</p>";
answerSet1 += "<div class=\"answerSelection\" id=\"answerSelection1\" ></div>";
answerSet1 += "<\/div>";
answerSet1 += "<div class=\"answer\"  id=\"answer2\" onclick=\"select('1,2')\">";
answerSet1 += "<p>" + items[3] + "</p>";
answerSet1 += "<div class=\"answerSelection\" id=\"answerSelection2\" ></div>";
answerSet1 += "<\/div>";
answerSet1 += "<div class=\"answer\"  id=\"answer3\" onclick=\"select('1,3')\">";
answerSet1 += "<p>" + items[4] + "</p>";
answerSet1 += "<div class=\"answerSelection\" id=\"answerSelection3\" ></div>";
answerSet1 += "<\/div>";

var titleQuestion2 = items[5];
var question2 = items[6];

var answerSet2 = "";
answerSet2 += "<div class=\"answer answerMiniTxt\"  id=\"answer1\" onclick=\"select('2,1')\">";
answerSet2 += "<p>" + items[7] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection1\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\"  id=\"answer2\" onclick=\"select('2,2')\">";
answerSet2 += "<p>" + items[8] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection2\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\"  id=\"answer3\" onclick=\"select('2,3')\">";
answerSet2 += "<p>" + items[9] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection3\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\"  id=\"answer4\" onclick=\"select('2,4')\">";
answerSet2 += "<p>" + items[10] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection4\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\"  id=\"answer5\" onclick=\"select('2,5')\">";
answerSet2 += "<p>" + items[11] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection5\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\"  id=\"answer6\" onclick=\"select('2,6')\">";
answerSet2 += "<p>" + items[12] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection6\" ></div>";
answerSet2 += "<\/div>";

var titleQuestion31 = items[13];
var titleQuestion32 = items[14];
var question3 = items[15];

var question31 = items[16];
var question32 = items[17];

var answerSet3 = "";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer1\" onclick=\"select('3,1')\">";
answerSet3 += "<p>1. " + items[18] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection1\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer2\" onclick=\"select('3,2')\">";
answerSet3 += "<p>2. " + items[19] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection2\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer3\" onclick=\"select('3,3')\">";
answerSet3 += "<p>3. " + items[20] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection3\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer4\" onclick=\"select('3,4')\">";
answerSet3 += "<p>4. " + items[21] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection4\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer5\" onclick=\"select('3,5')\">";
answerSet3 += "<p>5. " + items[22] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection5\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer6\" onclick=\"select('3,6')\">";
answerSet3 += "<p>6. " + items[23] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection6\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer7\" onclick=\"select('3,7')\">";
answerSet3 += "<p>7. " + items[24] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection7\" ></div>";
answerSet3 += "<\/div>";

var titleQuestion4 = items[25];
var question4 = items[26];

var answerSet4 = "";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer1\" onclick=\"select('4,1')\">";
answerSet4 += "<p>1. " + items[27] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection1\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer2\" onclick=\"select('4,2')\">";
answerSet4 += "<p>2. " + items[28] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection2\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer3\" onclick=\"select('4,3')\">";
answerSet4 += "<p>3. " + items[29] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection3\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer4\" onclick=\"select('4,4')\">";
answerSet4 += "<p>4. " + items[30] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection4\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer5\" onclick=\"select('4,5')\">";
answerSet4 += "<p>5. " + items[31] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection5\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer6\" onclick=\"select('4,6')\">";
answerSet4 += "<p>6. " + items[32] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection6\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer7\" onclick=\"select('4,7')\">";
answerSet4 += "<p>7." + items[33] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection7\" ></div>";
answerSet4 += "<\/div>";

String.prototype.replaceAt = function (index, character) {
    return this.substr(0, index) + character + this.substr(index + character.length);
}