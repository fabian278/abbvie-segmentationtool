var items = [];

//First question title
items.push("Question 1");

//First question
items.push("Please select one of the following statements that best reflects your level of specialization within oncology \/ hematology.");

//Answer ser for first question
items.push("I consider myself a generalist within my chosen specialty of oncology \/ hematology.");
items.push("I have a specific area of focus within oncology \/ hematology that includes Chronic Lymphocytic Leukemia (CLL).");
items.push("I have a specific area of focus within oncology \/ hematology that DOES NOT include CLL.");

//Second question title
items.push("Question 2");

//Second question
items.push("Please select from the following options how frequently are you involved in determining local / hospital guidelines or clinical protocols.");
//Answer ser for second question
items.push("Once per month");
items.push("Every 2 - 3 months");
items.push("Every 4 - 6 months");
items.push("Once a year");
items.push("Less frequently than once per year");
items.push("Never");

//Third question title
items.push("Question 3.1");
items.push("Question 3.2");

//Third question
items.push("Please rate your level of agreement with this statement on the following scale.");

//Subquestion 3.1
items.push("My peers look to me as an expert in treating relapsed / refractory CLL patients.");

//Subquestion 3.1
items.push("I may refer relapsed / refractory CLL patients to other physicians for treatment.");

//Answer ser for third question
items.push("Completely disagree");
items.push("Strongly disagree");
items.push("Slightly disagree");
items.push("Neither agree nor disagree");
items.push("Slightly agree");
items.push("Strongly agree");
items.push("Completely agree");

//Fourth question title
items.push("Question 4");

//Fourth question
items.push("Please rate how important developing long term relationships with your patients is to you in terms of your day to day satisfaction with your role as a physician on the following scale.");

//Answer ser for fourth question
items.push("Extremely unimportant");
items.push("Very unimportant");
items.push("Slightly unimportant");
items.push("Neither important nor unimportant");
items.push("Slightly important");
items.push("Very important");
items.push("Extremely important"); //Index = 33

//Wellcome text Index = 34
items.push("Welcome to Venetoclax ProFILES Segmentation Tool, an interactive platform, that will allow you to classify HCP archetypes in your market, uncovering their behavioral patterns, professional preferences, and engagement drivers.");
//Begin message Index = 35
items.push("START SEGMENTATION");

//Next button text Index = 36
items.push("NEXT");

//Previous button text Index = 37
items.push("BACK");

//Finish button text Index = 38
items.push("SUBMIT");

//Alert message text (Displayed when user tries to move to the next question without selecting an answer) Index = 39
items.push("Please select an answer to continue.");

//Index = 40
items.push("Thanks for using the ProFILES Segmentation Tool!");

//Index = 41
items.push("Your HCP segment is: ")

//Index = 42
items.push("FINALIZE SEGMENTATION");

//Index = 43
items.push("Please select an Account and choose the 'Launch Media' option in order to start segmentation.");

//Index = 44
items.push("EXIT");

//Index = 45
items.push("Analytical Commander");

//Index = 46
items.push("Habit-driven Conservative");

//Index = 47
items.push("Expert Trendsetter");

//Index = 48
items.push("Compassionate Follower");
//Index = 49
items.push("Behavioral information about each segment is based on the global market research conducted by AbbVie. For internal use only. Company confidential.");

//Index = 50
items.push("Prof. Thomas Prequel, MD, PhD");
//Index = 51
items.push("Andrzej Ostrozny, MD");
//Index = 52
items.push("Prof. Klaus Gebieter, MD, PhD");
//Index = 43
items.push("Joao Adepto, MD");
 