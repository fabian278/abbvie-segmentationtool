
/* Created 06/07/2015 by Fabian Salamo  @Yama Group Montevideo */

//Define arrays for each segment:
//Arrays positions correspond to questions in this way: Q1, Q2, Q3_1, Q3_2, Q4

var segment1 = [-3.210707937, -0.45373745, -3.243144087, 0.503810151, -4.320928815];

var segment2 = [0.463654789, 0.42327923, -5.024763178, 0.934925174, -5.282146356];

var segment3 = [-1.893207684, -0.3807725, -2.412338731, 1.887851453, -4.421527587];

var segment4 = [-0.473957787, 0.628190746, -6.0500458, 0.747805451, -3.571555571];

var segmentConstants = [50.31495149, 56.43566531, 40.08208995, 51.2739782];

var currentQuestion = 1;
var partialAnswers = '';
var canContinue = false;

//This method calculates the value for the segment depending on the submitted answers
function getSegment1() {
    var sum = 0;
    var answersArray = partialAnswers.substring(0, partialAnswers.length - 1).split('-');
    console.log('answersArray: '+answersArray);
    for (var i = 0; i < answersArray.length; i++) {
        sum += answersArray[i] * segment1[i];
        /*
        console.log('i: '+i);
        console.log('answersArray[i]: '+answersArray[i]);
        console.log('segment1[i]: '+segment1[i]);
        console.log('answersArray[i] * segment1[i]: ' + answersArray[i] * segment1[i]);
        console.log('sum: ' + sum);
        console.log('sum + segmentConstants[0]: ' + sum + segmentConstants[0]);
        console.log('Math.exp((sum + segmentConstants[0])): ' + Math.exp((sum + segmentConstants[0])));
        */
    }
    return Math.exp((sum + segmentConstants[0]));
}

function getSegment2() {
    var sum = 0;
    var answersArray = partialAnswers.substring(0, partialAnswers.length - 1).split('-');
    for (var i = 0; i < answersArray.length; i++) {
        sum += answersArray[i] * segment2[i];
    }
    return Math.exp((sum + segmentConstants[1]));
}

function getSegment3() {
    var sum = 0;
    var answersArray = partialAnswers.substring(0, partialAnswers.length - 1).split('-');
    for (var i = 0; i < answersArray.length; i++) {
        sum += answersArray[i] * segment3[i];
    }
    return Math.exp((sum + segmentConstants[2]));
}

function getSegment4() {
    var sum = 0;
    var answersArray = partialAnswers.substring(0, partialAnswers.length - 1).split('-');
    for (var i = 0; i < answersArray.length; i++) {
        sum += answersArray[i] * segment4[i];
    }
    return Math.exp((sum + segmentConstants[3]));
}


//question 1 takes values 1 or 0 (first option =1, else =0)
//question 2 takes values 1 - 6 
//question 3.1 takes values 1 - 7 
//question 3.2 takes values 1 - 7 
//question 4 takes values 1 - 7 

function getAnswersArray() {
    var arr = "1-2-3-4-5-";
    return arr.split(',');
}

function next() {
    //fade out container
    if (canContinue) {
        saveAnswer();
        $(".mainContainer").fadeOut(700, function () {
            if ($("#nextButton").text() == "Submit") {
                showSegmentation();

                alert("Thanks for using the segmentation tool!");
            } else {
                $("#prevButton").show();
                //set new content
                if (currentQuestion == 1) {
                    $(".questionContainer").html(question2);
                    $(".answerContainer").html(answerSet2);
                }
                if (currentQuestion == 2) {//for Q 3.1
                    $(".questionContainer").html(question3);
                    //sub-question 3.1
                    $(".subquestionContainer").show();
                    $(".subquestionContainer").html(question31);
                    $(".answerContainer").html(answerSet3);
                }
                if (currentQuestion == 3) { //for Q 3.2
                    $(".questionContainer").html(question3);
                    //sub-question 3.2
                    $(".subquestionContainer").show();
                    $(".subquestionContainer").html(question32);
                    $(".answerContainer").html(answerSet3);
                }
                if (currentQuestion == 4) {
                    $(".subquestionContainer").hide();
                    $(".questionContainer").html(question4);
                    $(".answerContainer").html(answerSet4);
                }
                currentQuestion = currentQuestion + 1;

                if (currentQuestion > 4) {
                    $("#nextButton").text("Submit");
                }
                //fade in container
                $(".mainContainer").fadeIn();
            }
        });
        canContinue = false;
    } else {
        alert("You must answer this question in order to continue!")
    }
    
    console.log("currentQuestion: " + currentQuestion);
    console.log("partialAnswers: " + partialAnswers);
}

function prev() {
    //fade out container
    $(".mainContainer").fadeOut(700, function () {
        $("#nextButton").text("Next");
        $("#nextButton").show();
        //set new content
        if (currentQuestion == 2) {

            

            $(".questionContainer").html(question1);
            $(".answerContainer").html(answerSet1);
        }
        if (currentQuestion == 3) {//for Q 3.1
            $(".questionContainer").html(question2);
            $(".subquestionContainer").hide();
            $(".answerContainer").html(answerSet2);
        }
        if (currentQuestion == 4) { //for Q 3.2
            $(".questionContainer").html(question3);
            //sub-question 3.2
            $(".subquestionContainer").html(question31);
            $(".answerContainer").html(answerSet3);
        }
        if (currentQuestion == 5) {
            $(".subquestionContainer").show();
            $(".questionContainer").html(question3);
            $(".subquestionContainer").html(question32);
            $(".answerContainer").html(answerSet3);
        }
        currentQuestion = currentQuestion - 1;

        if (currentQuestion == 1) {
            $("#prevButton").hide();
        }
        //fade in container
        $(".mainContainer").fadeIn();
        //var sel = partialAnswers[partialAnswers.length - 2];
        //var aux = partialAnswers.substring(0, partialAnswers.length - 2);
        //partialAnswers = aux;
        //selectAnswer(sel);
    });
    console.log("currentQuestion: " + currentQuestion);
    console.log("partialAnswers: " + partialAnswers);
}

function select(selected) {
    canContinue = true;
    var aux = selected.split(',')[1];
    //Sets the background for the selected item
    selectAnswer(aux);
    
    console.log('Partial answers: ' + partialAnswers);
}

function saveAnswer() {
    var elem = $(".selected").attr("id")[6];
    if (partialAnswers.length < 1) {
        partialAnswers = $("#partialResult").val();
    }
    partialAnswers += elem + '-';
}

function unSelect() {
    var partialAnswers = $("#partialResult").val();
    partialAnswers += selected + '-';
    console.log('Partial answers: ' + partialAnswers);
}

function selectAnswer(selected) {
    var answer = 'answer' + selected;
    for (var i = 0; i < 8; i++) {
        $("#answer" + i).removeClass("selected");
    }
    $("#" + answer).addClass("selected");
}

function showSegmentation() {
    //partialAnswers = "1-2-3-4-5-";

    var s1 = getSegment1();
    var s2 = getSegment2();
    var s3 = getSegment3();
    var s4 = getSegment4();

    var total = s1 + s2 + s3 + s4;
    var p1 = Math.round((s1 * 100) / total);
    var p2 = Math.round((s2 * 100) / total);
    var p3 = Math.round((s3 * 100) / total);
    var p4 = Math.round((s4 * 100) / total);
   
    $("#p1").css('width', p1 + '%');
    $("#p2").css('width', p2 + '%');
    $("#p3").css('width', p3 + '%');
    $("#p4").css('width', p4 + '%');

    $("#p1").text(p1 + '%');
    $("#p2").text(p2 + '%');
    $("#p3").text(p3 + '%');
    $("#p4").text(p4 + '%');

    var max = 0;
    var segment = 0;
    var arr = [p1, p2, p3, p4];
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
            segment = i + 1;
        }
    }

    $("#segment").text('Segment: ' + segment);

  

    $(".mainContainer").fadeIn();
    $(".questionContainer").hide();
    $(".answerContainer").hide();
    $(".buttonsContainer").hide();
    $(".results").fadeIn();



    console.log('Segment: '+segment);

    console.log(p1 + "% - " + p2 + "% - " + p3 + "% - " + p4 + "%");
}

var question1 = "Please select one of the following statements that best reflects your level of specialization within oncology \/ hematology";

var answerSet1 = "";
answerSet1 += "<div class=\"answer\"  id=\"answer1\" onclick=\"select('1,1')\">";
answerSet1 += "I consider myself a generalist within my chosen specialty of oncology\/hematology.";
answerSet1 += "<\/div>";
answerSet1 += "<div class=\"answer\"  id=\"answer2\" onclick=\"select('1,0')\">";
answerSet1 += "I have a specific area of focus within oncology \/ hematology that includes Chronic lymphocytic leukemia (CLL).";
answerSet1 += "<\/div>";
answerSet1 += "<div class=\"answer\"  id=\"answer3\" onclick=\"select('1,0')\">";
answerSet1 += "I have a specific area of focus within oncology \/ hematology that DOES NOT include Chronic lymphocytic leukemia (CLL).";
answerSet1 += "<\/div>";

var question2 = "How frequently are you involved in Determining local/hospital guidelines or clinical protocols, please select one of the following options:";

var answerSet2 = "";
answerSet2 += "<div class=\"answer\"  id=\"answer1\" onclick=\"select('2,1')\">";
answerSet2 += "Once per month";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer\"  id=\"answer2\" onclick=\"select('2,2')\">";
answerSet2 += "Every 2 - 3 months";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer\"  id=\"answer3\" onclick=\"select('2,3')\">";
answerSet2 += "Every 4 - 6 months";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer\"  id=\"answer4\" onclick=\"select('2,4')\">";
answerSet2 += "Once a year";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer\"  id=\"answer5\" onclick=\"select('2,5')\">";
answerSet2 += "Less frequently than once per year";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer\"  id=\"answer6\" onclick=\"select('2,6')\">";
answerSet2 += "Never";
answerSet2 += "<\/div>";

var question3 = "Please rate your level of agreement with each of the statements I read out, on the following scale";

var question31 = "My peers look to me as an expert in treating relapsed / refractory CLL patients";
var question32 = "I may refer relapsed/refractory CLL patients to other physicians for treatment";

var answerSet3 = "";
answerSet3 += "<div class=\"answer\" id=\"answer1\" onclick=\"select('3,1')\">";
answerSet3 += "Completely disagree";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer\" id=\"answer2\" onclick=\"select('3,2')\">";
answerSet3 += "Strongly disagree";
answerSet3 += "<\/div><div class=\"answer\" id=\"answer3\" onclick=\"select('3,3')\">";
answerSet3 += "Slightly disagree";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer\" id=\"answer4\" onclick=\"select('3,4')\">";
answerSet3 += "Neither agree nor disagree";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer\" id=\"answer5\" onclick=\"select('3,5')\">";
answerSet3 += "Slightly agree";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer\" id=\"answer6\" onclick=\"select('3,6')\">";
answerSet3 += "Strongly agree";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer\" id=\"answer7\" onclick=\"select('3,7')\">";
answerSet3 += "Completely agree";
answerSet3 += "<\/div>";


var question4 = "Please rate how important developing long term relationships with your patients is to you in terms of your day to day satisfaction with your role as a physician on the following scale:";

var answerSet4 = "";
answerSet4 += "<div class=\"answer\" id=\"answer1\" onclick=\"select('4,1')\">";
answerSet4 += "Extremely unimportant";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer\" id=\"answer2\" onclick=\"select('4,2')\">";
answerSet4 += "Very unimportant";
answerSet4 += "<\/div><div class=\"answer\" id=\"answer3\" onclick=\"select('4,3')\">";
answerSet4 += "Slightly unimportant";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer\" id=\"answer4\" onclick=\"select('4,4')\">";
answerSet4 += "Neither important nor unimportant";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer\" id=\"answer5\" onclick=\"select('4,5')\">";
answerSet4 += "Slightly important";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer\" id=\"answer6\" onclick=\"select('4,6')\">";
answerSet4 += "Very important";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer\" id=\"answer7\" onclick=\"select('4,7')\">";
answerSet4 += "Extremely important";
answerSet4 += "<\/div>";


String.prototype.replaceAt = function (index, character) {
    return this.substr(0, index) + character + this.substr(index + character.length);
}